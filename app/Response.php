<?php

namespace app;

/**
 * Class Response
 * @package app
 *
 * @todo remove echo from responce;
 * @todo need class hierarchy for many types of Response
 * @todo need handle response code and headers
 */
class Response{

    public function __construct(string $response)
    {
        header('Content-Type: application/json');
        echo $response;
    }

}