<?php

namespace app;

/**
 * Class Router
 * @package app
 *
 * @todo need handle request method in router
 * @todo need firewall
 */
class Router{

    private $configuration = [
        [
            'pattern' => '^/api/v[0-9].*/product$',
            'controller' => 'ProductController',
            'action' => 'getList',
        ],
        [
            'pattern' => '^/api/v[0-9].*/product/([a-z0-9\-]*)$',
            'controller' => 'ProductController',
            'action' => 'getByUuid',
        ]
    ];

    /**
     * Router constructor.
     * @todo need to shield ~ symbol in pattern
     */
    public function __construct($path)
    {
        foreach ($this->configuration as $configuration){
            preg_match('~'. $configuration['pattern'] . '~', $path, $matches);
            if (count($matches) > 0){
                $controllerName = '\Controller\\' . $configuration['controller'];
                $controller = new $controllerName();
                unset($matches[0]);
                call_user_func_array([$controller, $configuration['action']],$matches);
                exit(0);
            }
        }
    }

}